### In use Libs
   - Symfony 3.7
   - Doctrine MongoDB Bundle
   - FOS Rest Bundle (friendsofsymfony/rest-bundle)
   - JMSSerializerBundle (jms/serializer-bundle)
   - NelmioCorsBundle (nelmio/cors-bundle)
   - SolutionMongoAggregationBundle (solution-mongo-aggregation-bundle)
---
### Installing deendencies
    $ cd isofleettestserver
    $ composer install
### adding mongodb endpoint
    # app/config/parameters.yml
    parameters:
        mongodb_server: "mongodb://localhost:27017"
### Generating Database
    $ php bin/console doctrine:mongodb:schema:create
### Running the server
    $ php bin/console server:run
 
