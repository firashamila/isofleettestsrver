<?php

namespace AppBundle\Controller;


use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;

class RestaurantController extends FOSRestController
{


    /**
     * @Rest\Get("/api/restaurant/{id}")
     * method for getting a restaurant by ID
     */
    public function getRestaurantAction($id,Request $request){
        $restaurant = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Restaurant')
            ->find($id);
        if ($restaurant === null) {
            return new View(" No Restaurants", Response::HTTP_NOT_FOUND);
        }
        return $restaurant;
    }

    /**
     * @Rest\Get("/api/restaurant")
     * Method that returns all restaurants
     *
     */
    public function getRestaurantsAction(){
        $restaurants = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Restaurant')
            ->findAll();
        if ($restaurants === null) {
            return new View(" No Restaurants", Response::HTTP_NOT_FOUND);
        }
        return $restaurants;
    }

    /**
     * @Rest\Put("/api/restaurant")
     * Method that validates a restaurent
     */
    public function updateRestaurantAction(Request $request) {
        $id = $request->get('id');
        $em = $this->get('doctrine_mongodb')->getManager();
        $restaurant = $this->get('doctrine_mongodb')->getRepository('AppBundle:Restaurant')->find($id);
        if (empty($restaurant)) {
            return new View("Restaurant not found", Response::HTTP_NOT_FOUND);
        }
        else{
            $restaurant->setIsActive(true);
            $em->flush();
            $data=[
                'message' => 'Restaurent Accepted',
                'activatedRestaurant' => $restaurant
            ];
            return new View($data, Response::HTTP_OK);
        }
    }

    /**
     * @Rest\Delete("/api/restaurant")
     */
    public function deleteRestaurantAction(Request $request) {
        $id = $request->get('id');
        $repository = $this->get('doctrine_mongodb')->getRepository('AppBundle:Restaurant');
        $restaurant = $repository->find($id);
        if (empty($restaurant)) {
            return new View("restaurant not found", Response::HTTP_NOT_FOUND);
        }
        else {
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->remove($restaurant);
            $em->flush();
        }
        $data=[
            'message' => 'restaurant deleted successfully',
            'deletedRestaurant' => $restaurant
        ];
        return new View($data, Response::HTTP_OK);
    }


}
