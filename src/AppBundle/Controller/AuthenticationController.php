<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Document\Restaurant;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;

class AuthenticationController extends FOSRestController
{
    /**
     * @Rest\Post("/api/restaurant")
     * Method for adding a restaurent
     */
    public function postRestaurantAction(Request $request) {
        $restaurant = new Restaurant;
        $username = $request->get('username');
        $password = $request->get('password');
        $name = $request->get('name');
        $description = $request->get('description');
        $openingFrom = $request->get('openingFrom');
        $OpeningTo = $request->get('OpeningTo');
        $address = $request->get('address');
        $website = $request->get('website');
        if(empty($name) || empty($username) || empty($password))
        {
            return new View("NULL username or credentials VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $restaurant->setName($name);
        $restaurant->setUsername($username);
        $restaurant->setPassword($password);
        $restaurant->setDescription($description);
        $restaurant->setOpeningFrom($openingFrom);
        $restaurant->setOpeningTo($OpeningTo);
        $restaurant->setAddress($address);
        $restaurant->setWebsite($website);
        $em = $this->get('doctrine_mongodb')->getManager();
        $em->persist($restaurant);
        $em->flush();
        $data = [
            'message' => 'Restaurant added successfully',
            'data' => $restaurant
        ];
        return new View($data, Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/api/restaurant/auth")
     * Method for adding a restaurent
     */
    public function restaurantSigneUpAction(Request $request) {
        $username = $request->get('username');
        $password = $request->get('password');

        if( empty($username) || empty($password))
        {
            return new View("NULL username or credentials VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $restaurant = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Restaurant')
            ->findOneBy(['username' => $username, 'password' => $password]);
        if($restaurant == null){
            return new View([], Response::HTTP_BAD_REQUEST);
        }
        return new View($restaurant, Response::HTTP_OK);
    }


}
