<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Solution\MongoAggregationBundle\SolutionMongoAggregationBundle;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Document\Item;


class ItemController extends FOSRestController
{
    /**
     * @Rest\Post("/api/item")
     * Method for adding an Item associated to a restaurent
     */
    public function postItemAction(Request $request) {
        $item = new Item;
        $name = $request->get('name');
        $description = $request->get('description');
        $price = $request->get('price');
        $restaurantId = $request->get('restaurantId');
        if(empty($name) || empty($restaurantId))
        {
            return new View("NULL name or restaurentId VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $item->setName($name);
        $item->setDescription($description);
        $item->setPrice($price);
        $item->setRestaurantId($restaurantId);
        $em = $this->get('doctrine_mongodb')->getManager();
        $em->persist($item);
        $em->flush();
        $data = [
            'message' => 'Item added successfully',
            'data' => $item
        ];
        return new View($data, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/api/item/{id}")
     * Gets Iems by restaurentId
     *
     */
    public function getItemsByRestaurantIdAction($id){
        $items = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Item')
            ->findBy(['restaurantId' => $id]);
        if ($items === null) {
            return new View(" No Items Found", Response::HTTP_NOT_FOUND);
        }
        return $items;
    }


    /**
     * @Rest\Delete("/api/item")
     * Deletes an Item with a given Id
     */
    public function deleteItemAction(Request $request) {
        $id = $request->get('id');
        $repository = $this->get('doctrine_mongodb')->getRepository('AppBundle:Item');
        $item = $repository->find($id);
        if (empty($item)) {
            return new View("restaurant not found", Response::HTTP_NOT_FOUND);
        } else {
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->remove($item);
            $em->flush();
        }
        $data = [
            'message' => 'restaurant deleted successfully',
            'deletedItem' => $item
        ];
        return new View($data, Response::HTTP_OK);
    }
}
